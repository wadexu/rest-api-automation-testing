package com.demo.qa.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import com.demo.qa.utils.RecordHandler;

public class Utils {

	public static Map<String, RecordHandler> sortmap(Map<String, RecordHandler> map) {

		Map<String, RecordHandler> sortmap = new TreeMap<String, RecordHandler>(
				new Comparator<String>() {
					public int compare(String obj1, String obj2) {
						return Integer.parseInt(obj1)-(Integer.parseInt(obj2));
					}
				});
		for (Map.Entry<String, RecordHandler> entry : map.entrySet()) {
			sortmap.put(entry.getKey(), entry.getValue());
		}

		return sortmap;
	}
}
