package com.demo.qa.utils;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SheetUtils {

	public static void removeSheetByName(XSSFWorkbook wb, String sheetName) {

		if (wb.getSheet(sheetName) != null) {

			for (int numSheet = 0; numSheet < wb.getNumberOfSheets(); numSheet++) {
				XSSFSheet sheet = wb.getSheetAt(numSheet);
				String name = sheet.getSheetName();

				if (name.equalsIgnoreCase(sheetName)) {
					wb.removeSheetAt(numSheet);
				}
			}
		}
	}

	public static CellStyle backgroundStyle(XSSFWorkbook wb, IndexedColors color) {
		CellStyle style = wb.createCellStyle();
		style.setFillForegroundColor(color.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);

		return style;
	}

	public static CellStyle backgroundStyle(XSSFWorkbook wb) {
		CellStyle style = wb.createCellStyle();
		style.setFillPattern(CellStyle.NO_FILL);

		return style;
	}

	public static XSSFFont font(XSSFWorkbook wb, String fontStr, int size) {
		XSSFFont font = wb.createFont();
		font.setFontName(fontStr);
		font.setFontHeightInPoints((short) size);

		return font;
	}

	public static void autoSizeColumn(XSSFSheet sheet, int colNum) {
		for (int k = 0; k < colNum; k++) {
			int origColWidth = sheet.getColumnWidth(k);
			sheet.autoSizeColumn(k);
			// Making sure col width is not going less than the header width
			if (origColWidth > sheet.getColumnWidth(k)) {
				sheet.setColumnWidth(k, origColWidth);
			}
		}
	}

	public static void addBorders(CellStyle style) {
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
	}
}