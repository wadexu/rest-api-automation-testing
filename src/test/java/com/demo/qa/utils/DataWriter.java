package com.demo.qa.utils;

import java.text.DecimalFormat;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.skyscreamer.jsonassert.FieldComparisonFailure;
import org.skyscreamer.jsonassert.JSONCompareResult;

public class DataWriter {
	
	public static void writeData(XSSFSheet comparison, String baseline, String actual, String ID, String test_case) {
		int rowNum = comparison.getLastRowNum();

		XSSFRow row = comparison.createRow(rowNum + 1);
		XSSFRow baseRow = comparison.createRow(rowNum + 2);
		
		CellStyle style1 = SheetUtils.backgroundStyle(comparison.getWorkbook(), IndexedColors.WHITE);
		XSSFFont font1 = SheetUtils.font(comparison.getWorkbook(), "Arial", 10);
		style1.setFont(font1);
		
		CellStyle style2 = SheetUtils.backgroundStyle(comparison.getWorkbook(), IndexedColors.LIGHT_CORNFLOWER_BLUE);
		XSSFFont font2 = SheetUtils.font(comparison.getWorkbook(), "Arial", 10);
		style2.setFont(font2);
		
		CellStyle style3 = SheetUtils.backgroundStyle(comparison.getWorkbook());
		XSSFFont font = SheetUtils.font(comparison.getWorkbook(), "Arial", 10);
		style3.setFont(font);
		
		XSSFCell xssfCell = row.createCell(3);
		XSSFCell xssfCellbase = baseRow.createCell(3);
		
		xssfCell.setCellValue(actual);
		xssfCellbase.setCellValue(baseline);
		
		xssfCell.setCellStyle(style1);
		xssfCellbase.setCellStyle(style2);
		
		// write ID, test case and Assert
		XSSFCell xssfCell_0 = row.createCell(0);
		XSSFCell xssfCell_1 = row.createCell(1);
		XSSFCell xssfCell_2 = row.createCell(2);
		XSSFCell xssfCell_3 = baseRow.createCell(2);

		xssfCell_0.setCellValue(ID);
		xssfCell_0.setCellStyle(style3);
		xssfCell_1.setCellValue(test_case);
		xssfCell_1.setCellStyle(style3);
		xssfCell_2.setCellValue("Actual");
		xssfCell_2.setCellStyle(style1);
		xssfCell_3.setCellValue("Expect");
		xssfCell_3.setCellStyle(style2);

		// write header
		if (rowNum < 1) {
			XSSFRow headerRow = comparison.createRow(0);
			XSSFCell headerCell_0 = headerRow.createCell(0);
			XSSFCell headerCell_1 = headerRow.createCell(1);
			XSSFCell headerCell_2 = headerRow.createCell(2);
			XSSFCell headerCell_3 = headerRow.createCell(3);

			headerCell_0.setCellValue("ID");
			headerCell_1.setCellValue("TestCase");
			headerCell_2.setCellValue("Assert");
			headerCell_3.setCellValue("Failure field:Value");
			
			headerCell_0.setCellStyle(style3);
			headerCell_1.setCellStyle(style3);
			headerCell_2.setCellStyle(style3);
			headerCell_3.setCellStyle(style3);
		}
		SheetUtils.autoSizeColumn(comparison, 10);
	}

	public static void writeData(XSSFSheet comparison, JSONCompareResult result, String ID, String test_case) {
		
		int rowNum = comparison.getLastRowNum();

		XSSFRow row = comparison.createRow(rowNum + 1);
		XSSFRow baseRow = comparison.createRow(rowNum + 2);
		
		CellStyle style1 = SheetUtils.backgroundStyle(comparison.getWorkbook(), IndexedColors.WHITE);
		XSSFFont font1 = SheetUtils.font(comparison.getWorkbook(), "Arial", 10);
		style1.setFont(font1);
		
		CellStyle style2 = SheetUtils.backgroundStyle(comparison.getWorkbook(), IndexedColors.LIGHT_CORNFLOWER_BLUE);
		XSSFFont font2 = SheetUtils.font(comparison.getWorkbook(), "Arial", 10);
		style2.setFont(font2);
		
		CellStyle style3 = SheetUtils.backgroundStyle(comparison.getWorkbook());
		XSSFFont font = SheetUtils.font(comparison.getWorkbook(), "Arial", 10);
		style3.setFont(font);
		
		if (result.getFieldFailures().isEmpty()) {
			XSSFCell xssfCell = row.createCell(3);
			XSSFCell xssfCellbase = baseRow.createCell(3);

			xssfCell.setCellValue(result.getMessage());
			xssfCellbase.setCellValue(result.getMessage());
			xssfCell.setCellStyle(style1);
			xssfCellbase.setCellStyle(style2);
		} else {
			List<FieldComparisonFailure> failureList = result.getFieldFailures();
			int totalCell = failureList.size();

		// write the failure data with expected data
		for (int cellNum = 3; cellNum < totalCell + 3; cellNum++) {
			XSSFCell xssfCell = row.createCell(cellNum);
			XSSFCell xssfCellbase = baseRow.createCell(cellNum);

			xssfCell.setCellValue(failureList.get(cellNum - 3).getField() + ":" + failureList.get(cellNum - 3).getActual());
			xssfCellbase.setCellValue(failureList.get(cellNum - 3).getField() + ":" + failureList.get(cellNum - 3).getExpected());
			
			xssfCell.setCellStyle(style1);
			xssfCellbase.setCellStyle(style2);
		    }
		
	    }

		// write ID, test case and Assert
		XSSFCell xssfCell_0 = row.createCell(0);
		XSSFCell xssfCell_1 = row.createCell(1);
		XSSFCell xssfCell_2 = row.createCell(2);
		XSSFCell xssfCell_3 = baseRow.createCell(2);

		xssfCell_0.setCellValue(ID);
		xssfCell_0.setCellStyle(style3);
		xssfCell_1.setCellValue(test_case);
		xssfCell_1.setCellStyle(style3);
		xssfCell_2.setCellValue("Actual");
		xssfCell_2.setCellStyle(style1);
		xssfCell_3.setCellValue("Expect");
		xssfCell_3.setCellStyle(style2);

		// write header
		if (rowNum < 1) {
			
			
			XSSFRow headerRow = comparison.createRow(0);
			XSSFCell headerCell_0 = headerRow.createCell(0);
			XSSFCell headerCell_1 = headerRow.createCell(1);
			XSSFCell headerCell_2 = headerRow.createCell(2);
			XSSFCell headerCell_3 = headerRow.createCell(3);

			headerCell_0.setCellValue("ID");
			headerCell_1.setCellValue("TestCase");
			headerCell_2.setCellValue("Assert");
			headerCell_3.setCellValue("Failure field:Value");
			
			headerCell_0.setCellStyle(style3);
			headerCell_1.setCellStyle(style3);
			headerCell_2.setCellStyle(style3);
			headerCell_3.setCellStyle(style3);
		}

		SheetUtils.autoSizeColumn(comparison, 10);
	}

	public static void writeData(XSSFSheet output, String response, String ID, String test_case) {
		int rowNum = output.getLastRowNum();
		XSSFRow row = output.createRow(rowNum + 1);

	    XSSFCell xssfCell_0 = row.createCell(0);
	    XSSFCell xssfCell_1 = row.createCell(1);
	    XSSFCell xssfCell_2 = row.createCell(2);
	    
		xssfCell_0.setCellValue(ID);
		xssfCell_1.setCellValue(test_case);
		xssfCell_2.setCellValue(response);

		//color
		CellStyle style = SheetUtils.backgroundStyle(output.getWorkbook(), IndexedColors.WHITE);
		SheetUtils.addBorders(style);
		xssfCell_0.setCellStyle(style);
		xssfCell_1.setCellStyle(style);
		xssfCell_2.setCellStyle(style);
		
		// write header
		if (rowNum < 1) {
			XSSFRow headerRow = output.createRow(0);
			XSSFCell headerCell_0 = headerRow.createCell(0);
			XSSFCell headerCell_1 = headerRow.createCell(1);
			XSSFCell headerCell_2 = headerRow.createCell(2);

			CellStyle style1 = SheetUtils.backgroundStyle(output.getWorkbook());
			SheetUtils.addBorders(style1);
			
			headerCell_0.setCellValue("ID");
			headerCell_1.setCellValue("TestCase");
			headerCell_2.setCellValue("Response");

			headerCell_0.setCellStyle(style1);
			headerCell_1.setCellStyle(style1);
			headerCell_2.setCellStyle(style1);
		}
		output.setColumnWidth(0, 1500);
		output.setColumnWidth(1, 4000);
		output.setColumnWidth(2, 65000);
	}
	
	public static void writeData(XSSFSheet output) {
		CellStyle style = SheetUtils.backgroundStyle(output.getWorkbook(), IndexedColors.RED);
		SheetUtils.addBorders(style);
		
		int rowNum = output.getLastRowNum();
		XSSFRow row = output.getRow(rowNum);
		XSSFCell cell = row.getCell(1);
		cell.setCellStyle(style);
	}
	
	public static void writeData(XSSFSheet sheet, String result, String ID, String test_case, int i) {
		int rowNum = sheet.getLastRowNum();
		XSSFRow row = sheet.createRow(rowNum + 1);
		CellStyle style1 = SheetUtils.backgroundStyle(sheet.getWorkbook());
		XSSFFont font2 = SheetUtils.font(sheet.getWorkbook(), "Arial", 10);
		style1.setFont(font2);
		
		CellStyle style2 = SheetUtils.backgroundStyle(sheet.getWorkbook(), IndexedColors.RED);

		XSSFCell xssfCell_0 = row.createCell(0);
		XSSFCell xssfCell_1 = row.createCell(1);
		XSSFCell xssfCell_2 = row.createCell(2);

		xssfCell_0.setCellValue(ID);
		xssfCell_0.setCellStyle(style1);
		xssfCell_1.setCellValue(test_case);
		xssfCell_1.setCellStyle(style1);

		if (result.equalsIgnoreCase("true")) {
			xssfCell_2.setCellValue("PASSED");
			xssfCell_2.setCellStyle(style1);
		} else if (result.equalsIgnoreCase("false")) {
			xssfCell_2.setCellValue("FAILED");
			xssfCell_2.setCellStyle(style2);
		} else {
			xssfCell_2.setCellValue("ERROR");
			xssfCell_2.setCellStyle(style2);
		}
		
		// write header
		if (rowNum < 1) {
			XSSFRow headerRow = sheet.createRow(0);
			XSSFCell headerCell_0 = headerRow.createCell(0);
			XSSFCell headerCell_1 = headerRow.createCell(1);
			XSSFCell headerCell_2 = headerRow.createCell(2);

			headerCell_0.setCellValue("ID");
			headerCell_1.setCellValue("TestCase");
			headerCell_2.setCellValue("Result");
			
			headerCell_0.setCellStyle(style1);
			headerCell_1.setCellStyle(style1);
			headerCell_2.setCellStyle(style1);
		}
	}
	
	public static void writeData(XSSFSheet resultSheet, double totalcase, double failedcase, String startTime, String endTime) {
		DecimalFormat decFormat = new DecimalFormat("##.00%"); 
		String passPercentage = decFormat.format((totalcase-failedcase)/totalcase);
		CellStyle style = SheetUtils.backgroundStyle(resultSheet.getWorkbook(), IndexedColors.GREY_25_PERCENT);
		XSSFFont font = SheetUtils.font(resultSheet.getWorkbook(), "Arial", 10);
		style.setFont(font);
		
		int rowNum = resultSheet.getLastRowNum();
		XSSFRow row1 = resultSheet.createRow(rowNum + 1);
		XSSFCell xssfCell_0 = row1.createCell(1);
		XSSFCell xssfCell_1 = row1.createCell(2);
		
		xssfCell_0.setCellValue("Pass Percentage:");
		xssfCell_1.setCellValue(passPercentage);
		
		xssfCell_0.setCellStyle(style);
		xssfCell_1.setCellStyle(style);
		
		XSSFRow row2 = resultSheet.createRow(rowNum + 2);
		XSSFCell xssfCell_00 = row2.createCell(1);
		XSSFCell xssfCell_01 = row2.createCell(2);
		
		xssfCell_00.setCellValue("Start Time:");
		xssfCell_01.setCellValue(startTime);
		
		xssfCell_00.setCellStyle(style);
		xssfCell_01.setCellStyle(style);
		
		XSSFRow row3 = resultSheet.createRow(rowNum + 3);
		XSSFCell xssfCell_000 = row3.createCell(1);
		XSSFCell xssfCell_001 = row3.createCell(2);
		
		xssfCell_000.setCellValue("End Time:");
		xssfCell_001.setCellValue(endTime);
		
		xssfCell_000.setCellStyle(style);
		xssfCell_001.setCellStyle(style);
		
		resultSheet.setColumnWidth(0, 1500);
		resultSheet.autoSizeColumn(1, false);
		resultSheet.setColumnWidth(2, 2000);
	}
}
