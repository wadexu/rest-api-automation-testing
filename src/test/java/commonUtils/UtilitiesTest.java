package commonUtils;

import org.junit.Assert;
import org.junit.Test;

public class UtilitiesTest {

    @Test
    public void reverse_normal() {
        String actual = Utilities.reverse("abcedfg");
        Assert.assertEquals("gfdecba", actual);
    }
    
    @Test
    public void reverse_null() {
        String actual = Utilities.reverse(null);
        Assert.assertEquals(null, actual);
    }

    @Test
    public void reverse_blank() {
        String actual = Utilities.reverse("");
        Assert.assertEquals("", actual);
    }
    
    @Test
    public void replace_normal() {
        String actual = Utilities.replace("Hellowarld!", "wa", " wo");
        Assert.assertEquals("Hello world!", actual);
    }
    
    @Test
    public void replace_multiple() {
        String actual = Utilities.replace("abcdefgabc", "abc", "test");
        Assert.assertEquals("testdefgtest", actual);
    }
    
    @Test
    public void replace_repetition() {
        String actual = Utilities.replace("aaaaa", "aa", "a");
        Assert.assertEquals("aaa", actual);
    }
    
    @Test
    public void replace_repetition1() {
        String actual = Utilities.replace("abcabc", "a", "aa");
        Assert.assertEquals("aabcaabc", actual);
    }
    
    @Test
    public void replace_toBlank() {
        String actual = Utilities.replace("Helloward!", "ward!", "");
        Assert.assertEquals("Hello", actual);
    }
    
    @Test
    public void replace_toNull() {
        String actual = Utilities.replace("Hello!", "!", null);
        Assert.assertEquals("Hello!", actual);
    }
    
    @Test
    public void replace_searchBlank() {
        String actual = Utilities.replace("Hello!", "", "test");
        Assert.assertEquals("Hello!", actual);
    }
    
    @Test
    public void replace_searchNull() {
        String actual = Utilities.replace("Hello!", null, "test");
        Assert.assertEquals("Hello!", actual);
    }
    
    @Test
    public void replace_sourceNull() {
        String actual = Utilities.replace(null, "abc", "test");
        Assert.assertEquals(null, actual);
    }
    
    @Test
    public void replace_sourceBlank() {
        String actual = Utilities.replace("", "abc", "test");
        Assert.assertEquals("", actual);
    }
    
    @Test
    public void remove_normal() {
        String actual = Utilities.remove("Hello worllld", "ll");
        Assert.assertEquals("Heo world", actual);
    }
    
    @Test
    public void remove_notFound() {
        String actual = Utilities.remove("Hello world", "abc");
        Assert.assertEquals("Hello world", actual);
    }
    
    @Test
    public void remove_null() {
        String actual = Utilities.remove("Hello world", null);
        Assert.assertEquals("Hello world", actual);
    }
    
    @Test
    public void remove_blank() {
        String actual = Utilities.remove("Hello world", "");
        Assert.assertEquals("Hello world", actual);
    }
    
    @Test
    public void remove_source_blank() {
        String actual = Utilities.remove("", "test");
        Assert.assertEquals("", actual);
    }
    
    @Test
    public void remove_source_null() {
        String actual = Utilities.remove(null, "test");
        Assert.assertEquals(null, actual);
    }
    
    @Test
    public void trim_normal() {
        String actual = Utilities.trim("     test       ");
        Assert.assertEquals("test", actual);
    }
    
    @Test
    public void trim_noSpace() {
        String actual = Utilities.trim("test");
        Assert.assertEquals("test", actual);
    }
    
    @Test
    public void trim_onlySpace() {
        String actual = Utilities.trim("      ");
        Assert.assertEquals("", actual);
    }
    
    @Test
    public void trim_blank() {
        String actual = Utilities.trim("");
        Assert.assertEquals("", actual);
    }
    
    @Test
    public void trim_null() {
        String actual = Utilities.trim(null);
        Assert.assertEquals(null, actual);
    }
    
    @Test //embeddedSpace will not be trimmed
    public void trim_embeddedSpace() {
        String actual = Utilities.trim("abc    efg");
        Assert.assertEquals("abc    efg", actual);
    }
    
    @Test
    public void startsWithIgnoreCase_normal() {
        boolean actual = Utilities.startsWithIgnoreCase("abcdef", "abc");
        Assert.assertEquals(true, actual);
    }
    
    @Test
    public void startsWithIgnoreCase_ignore() {
        boolean actual = Utilities.startsWithIgnoreCase("ABCDEF", "abc");
        Assert.assertEquals(true, actual);
    }
    
    @Test
    public void startsWithIgnoreCase_length() {
        boolean actual = Utilities.startsWithIgnoreCase("abc", "abcdefg");
        Assert.assertEquals(false, actual);
    }
    
    @Test
    public void startsWithIgnoreCase_null() {
        boolean actual = Utilities.startsWithIgnoreCase("ABCDEF", null);
        Assert.assertEquals(false, actual);
    }
    
    @Test
    public void startsWithIgnoreCase_blank() {
        boolean actual = Utilities.startsWithIgnoreCase("ABCDEF", "");
        Assert.assertEquals(true, actual);
    }
    
    @Test
    public void startsWithIgnoreCase_sourceNull() {
        boolean actual = Utilities.startsWithIgnoreCase(null, "abc");
        Assert.assertEquals(false, actual);
    }
    
    @Test
    public void startsWithIgnoreCase_sourceBlank() {
        boolean actual = Utilities.startsWithIgnoreCase("", "abc");
        Assert.assertEquals(false, actual);
    }
    
    //Two null references are considered to be equal.
    @Test
    public void startsWithIgnoreCase_allNull() {
        boolean actual = Utilities.startsWithIgnoreCase(null, null);
        Assert.assertEquals(true, actual);
    }
    
    //Two blank are considered to be equal.
    @Test
    public void startsWithIgnoreCase_allBlank() {
        boolean actual = Utilities.startsWithIgnoreCase("", "");
        Assert.assertEquals(true, actual);
    }
    
    //start with space
    @Test
    public void startsWithIgnoreCase_space1() {
        boolean actual = Utilities.startsWithIgnoreCase("   ", " ");
        Assert.assertEquals(true, actual);
    }
    
    //start with blank
    @Test
    public void startsWithIgnoreCase_space2() {
        boolean actual = Utilities.startsWithIgnoreCase("   ", "");
        Assert.assertEquals(true, actual);
    }
       
    @Test
    public void testNormalizeSpace() {
        String s = Utilities.normalizeSpace(" aaa bbb  ccc    ");
        Assert.assertEquals("aaa bbb ccc", s);
    }

    @Test
    public void testNormalizeSpaceNull() {
        String s = Utilities.normalizeSpace(null);
        Assert.assertEquals(null, s);
    }

    @Test
    public void testNormalizeSpaceBlank() {
        String s = Utilities.normalizeSpace("  ");
        Assert.assertEquals("", s);
    }
    
    @Test
    public void testNormalizeSpaceBlank1() {
        String s = Utilities.normalizeSpace("a ");
        Assert.assertEquals("a", s);
    }
    
    @Test
    public void testNormalizeSpaceBlank2() {
        String s = Utilities.normalizeSpace("a  b");
        Assert.assertEquals("a b", s);
    }
    
    @Test
    public void testNormalizeSpaceBlank3() {
        String s = Utilities.normalizeSpace("as                                                                               c                                                                                                   defe                                                            g");
        Assert.assertEquals("as c defe g", s);
    }
    
    @Test
    public void testPrependIfMissing_all_null() {
        String s = Utilities.prependIfMissing(null, null);
        Assert.assertEquals(null, s);
    }
    
    @Test
    public void testPrependIfMissing_prefix_null() {
        String s = Utilities.prependIfMissing("abc", null);
        Assert.assertEquals("abc", s);
    }
    
    @Test
    public void testPrependIfMissing_blank() {
        String s = Utilities.prependIfMissing("", "xyz");
        Assert.assertEquals("xyz", s);
    }
    
    @Test
    public void testPrependIfMissing_all_blank() {
        String s = Utilities.prependIfMissing("", "");
        Assert.assertEquals("", s);
    }
    
    @Test
    public void testPrependIfMissing_space() {
        String s = Utilities.prependIfMissing(" ", "   ");
        Assert.assertEquals("    ", s);
    }
    
    @Test
    public void testPrependIfMissing_normal() {
        String s = Utilities.prependIfMissing("abc", "xyz");
        Assert.assertEquals("xyzabc", s);
    }
    
    @Test
    public void testPrependIfMissing_duplicate() {
        String s = Utilities.prependIfMissing("xyzabc", "xyz");
        Assert.assertEquals("xyzabc", s);
    }
    
    @Test
    public void testPrependIfMissing_case_sensitive() {
        String s = Utilities.prependIfMissing("XYZabc", "xyz");
        Assert.assertEquals("xyzXYZabc", s);
    }
    
    @Test
    public void repeat_null() {
        String s = Utilities.repeat(null, 2);
        Assert.assertNull(s);
    }
    
    @Test
    public void repeat_blank() {
        String s = Utilities.repeat("", 2);
        Assert.assertEquals("", s);
    }

    @Test
    public void repeat_0times() {
        String s = Utilities.repeat("ab", 0);
        Assert.assertEquals("", s);
    }
    
    @Test
    public void repeat_negative() {
        String s = Utilities.repeat("ab", -2);
        Assert.assertEquals("", s);
    }

    @Test
    public void repeat_normal() {
        String s = Utilities.repeat("ab", 3);
        Assert.assertEquals("ababab", s);
    }
    
    @Test
    public void repeat_normal2() {
        String s = Utilities.repeat("a b", 3);
        Assert.assertEquals("a ba ba b", s);
    }
    
    @Test
    public void repeat_space() {
        String s = Utilities.repeat("  ", 3);
        Assert.assertEquals("      ", s);
    }
}
