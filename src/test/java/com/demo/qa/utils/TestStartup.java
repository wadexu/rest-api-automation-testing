/**
 * Standard Test startup
 */
package com.demo.qa.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

/**
 * This is the class to be used for all test programs running TestNG tests from the command line. The parent POM
 * stardard_test.pom points to this class in the profiles that make either the main jar or test jar.
 * 
 */
public final class TestStartup {

  /**
   * Static main function for all Command line Test applications.
   * 
   * @param args
   */
  public static void main(String[] args) {

    TestStartup startup = new TestStartup();
    int status = startup.run(args);

    System.out.println("Exiting with status:" + status);
    System.exit(status);
  }

  /**
   * Practically, this is the non-static main function. It takes the argument array from the command line.
   * 
   * @param args
   * @return 0 If there were no errors in the run. 1 If there were problems opening or running the given TestNG file. 2
   *         If there were problems parsing the command line arguments.
   */
  public int run(String[] args) {
    int status = 0;
    Options opts = null;
    try {
      opts = getOptions();

      CommandLine cmdLine = tryCommandLineParse(args, opts);

      if(cmdLine == null) {
        status = 2;
        usage(opts);
      } else {
        /* TestNG.main(args); */
        if(runTestNG(cmdLine)) {
          System.out.println("No errors reported.");
        } else {
          System.out.println("Errors found in run.");
          status = 1;
        }
      }
    } catch(Exception e) {
      status = 4;
      System.out.println("General Exception while parsing TestNG file.");
      e.printStackTrace();
    } catch(Throwable t) {
      status = 5;
      System.out.println("Error thrown:" + t.getMessage());
      t.printStackTrace();
    }
    return status;
  }

  /**
   * This is where the required options for all Command line Test applications are defined.
   * 
   * @return The set of options for the standard Test Command line interface.
   */
  private Options getOptions() {
    Options testStartupOptions = new Options();

    OptionBuilder.withLongOpt("testng");
    OptionBuilder.withArgName("<file>");
    OptionBuilder.isRequired();
    OptionBuilder.withValueSeparator();
    OptionBuilder.hasArg();
    OptionBuilder.withDescription("TestNG file to drive tests in this environment.");
    testStartupOptions.addOption(OptionBuilder.create("t"));

    return testStartupOptions;
  }

  /**
   * Takes the command line arguments and attempts to parse out the arguments specified in the list of CLI Options.
   * 
   * @param args The array of command line arugments
   * @param opts The CLI Options dictating the expected options
   * @return A CLI CommandLine representing the command line options passed by the user if the parse was successful. If
   *         there are any problems with the parse, returns null.
   */
  private CommandLine tryCommandLineParse(String[] args, Options opts) {
    CommandLine cmdLine = null;
    try {
      CommandLineParser clParser = new PosixParser();
      cmdLine = clParser.parse(opts, args);
    } catch(ParseException e) {
      System.out.println(e.getMessage());
    }

    return cmdLine;
  }

  /**
   * Takes the command line arguments (currently, only the testng file) and uses them to attempt to run the specified
   * tests.
   * 
   * @param cmdLine - The CLI CommandLine object with the name of the testng file to open up and run.
   * @return True if the testng file was successfully found, opened, and all tests were run. False if any problems
   *         happened in that process.
   */
  private boolean runTestNG(CommandLine cmdLine) throws ParserConfigurationException, SAXException, IOException {

    boolean bSuccess = false;
    try {
      System.out.println("Initializing parser");
      String strTestNGFile = cmdLine.getOptionValue("t");
      System.out.println("Parsing TestNG file:" + strTestNGFile);
      Parser xmlParser = new Parser(strTestNGFile);

      System.out.println("Parsing");
      ArrayList<XmlSuite> suiteList;
      suiteList = new ArrayList<XmlSuite>(xmlParser.parse());

      System.out.println("Attempting to run tests");
      TestNG test = new TestNG();
      test.setXmlSuites(suiteList);
      test.run();

      System.out.println("Run complete.");
      bSuccess = !test.hasFailure();
    } catch(ParserConfigurationException e) {
      System.out.println("Problem parsing TestNG file.");
      e.printStackTrace();
    } catch(SAXException e) {
      System.out.println("SAX Exception encountered while trying to parse TestNG file.");
      e.printStackTrace();
    } catch(FileNotFoundException e) {
      System.out.println("Could not find the TestNG file.");
    } catch(IOException e) {
      System.out.println("IOException while parsing TestNG file.");
      e.printStackTrace();
    }

    return bSuccess;
  }

  /**
   * Print a usage statement to the command line. The code attempts to glean the name of the jar that is being run from
   * the command line for the usage statement. If it cannot find it, it just uses <testjar> for the name.
   * 
   * @param opts CLI Options to use for printing out help
   */
  private void usage(Options opts) {
    String strJarName = null;
    try {
      strJarName = TestStartup.class.getProtectionDomain().getCodeSource().getLocation().getQuery();
    } catch(Throwable t) {
    }
    if(strJarName == null) {
      strJarName = "<testjar>";
    }

    if(opts != null) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp(strJarName, opts, true);
    } else {
      System.out.println("Error creating options.");
    }
  }
}
